import { Component, ViewChild, ElementRef, AfterViewInit  } from '@angular/core';

class Datos {
  construtor(figura = '', operacion = '', formula = '', valores = [], resultado = 0) {
    this.figura = figura;
    this.operacion = operacion;
    this.formula = formula;
    this.valores = valores;
    this.resultado = resultado;
  }
  figura: string;
  operacion: string;
  formula: string;
  valores: any[];
  resultado: number;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  
  @ViewChild('canvasEl', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;

  title = 'parametro-area';
  operacion: string;
  figura: string;
  valor_1: string;
  valor_2: string;
  valor_3: string;
  valor_4: string;
  valor_5: string;
  flag: boolean;
  lista_arreglo: any = [];
  obj: any;


   constructor () {
     this.operacion = 'Elegir operación...';
     this.figura = 'Elegir figura...';
     this.flag = false;
     this.limpiarInput();
    }

    ngAfterViewInit() {
      this.ctx = (this.canvas.nativeElement as HTMLCanvasElement).getContext('2d');
      // this.ctx = this.canvas.nativeElement.getContext('2d');
    }
    
    Onchange() {
      if(this.operacion !== 'Elegir operación...' && this.figura !== 'Elegir figura...'){
        this.flag = true;
        this.limpiarInput();

        this.ctx.clearRect(0,0, 160, 160);
        this.cuadricula();
        this.tipoDeFigura();
      } 
      else {
        this.flag = false;
        this.limpiarInput();

        this.ctx.clearRect(0,0, 160, 160);
      }
    }

    nombreFigura() {
      if(this.figura === 'cuadrado') return 'Cuadrado';
      else if(this.figura === 'rectangulo') return 'Rectángulo';
      else if(this.figura === 'rombo') return 'Rombo';
      else if(this.figura === 'trapecio_escaleno') return 'Trapecio escaleno';
      else if(this.figura === 'trapecio_isosceles') return 'Trapecio isósceles';
      else if(this.figura === 'triangulo_equilatero') return 'Triángulo equilatero';
      else if(this.figura === 'triangulo_escaleno') return 'Triángulo escaleno';
      else if(this.figura === 'triangulo_isosceles') return 'Triángulo isósceles';
      else if(this.figura === 'triangulo_rectangulo') return 'Triángulo rectángulo';
    }

    perimetros() {
      if(this.figura === 'cuadrado') return '4*L';
      else if(this.figura === 'rectangulo') return '(2*b) + (2*a)';
      else if(this.figura === 'rombo') return '4*L';
      else if(this.figura === 'trapecio_escaleno') return 'a + b + 2*c';
      else if(this.figura === 'trapecio_isosceles') return 'a + b + c + d';
      else if(this.figura === 'triangulo_equilatero') return '3*L';
      else if(this.figura === 'triangulo_escaleno') return 'a + b + c';
      else if(this.figura === 'triangulo_isosceles') return '(2*L)+b';
      else if(this.figura === 'triangulo_rectangulo') return 'a + b + c';
    }

    areas() {
      if(this.figura === 'cuadrado') return 'L*L';
      else if(this.figura === 'rectangulo') return 'b*a';
      else if(this.figura === 'rombo') return '(D*d)/2';
      else if(this.figura === 'trapecio_escaleno') return '(a+b)*h/2';
      else if(this.figura === 'trapecio_isosceles') return '(a+b)*h/2';
      else if(this.figura === 'triangulo_equilatero') return '(b*h)/2';
      else if(this.figura === 'triangulo_escaleno') return '(b*h)/2';
      else if(this.figura === 'triangulo_isosceles') return '(b*h)/2';
      else if(this.figura === 'triangulo_rectangulo') return '(b*h)/2';
    }

    valor1() {
      if(this.figura === 'cuadrado') return 'L';
      else if(this.figura === 'rectangulo') return 'b';
      else if(this.figura === 'rombo') return 'L';
      else if(this.figura === 'trapecio_escaleno') return 'a';
      else if(this.figura === 'trapecio_isosceles') return 'a';
      else if(this.figura === 'triangulo_equilatero') return 'L';
      else if(this.figura === 'triangulo_escaleno') return 'a';
      else if(this.figura === 'triangulo_isosceles') return 'L';
      else if(this.figura === 'triangulo_rectangulo') return 'a';
    }

    valor2() {
      if(this.figura === 'rectangulo') return 'a';
      else if(this.figura === 'rombo') return 'D';
      else if(this.figura === 'trapecio_escaleno') return 'b';
      else if(this.figura === 'trapecio_isosceles') return 'b';
      else if(this.figura === 'triangulo_equilatero') return 'h';
      else if(this.figura === 'triangulo_escaleno') return 'b';
      else if(this.figura === 'triangulo_isosceles') return 'b';
      else if(this.figura === 'triangulo_rectangulo') return 'b';
    }

    valor3() {
      if(this.figura === 'rombo') return 'd';
      else if(this.figura === 'trapecio_escaleno') return 'c';
      else if(this.figura === 'trapecio_isosceles') return 'c';
      else if(this.figura === 'triangulo_escaleno') return 'c';
      else if(this.figura === 'triangulo_isosceles') return 'h';
      else if(this.figura === 'triangulo_rectangulo') return 'c';
    }

    valor4() {
      if(this.figura === 'trapecio_escaleno') return 'd';
      else if(this.figura === 'trapecio_isosceles') return 'h';
      else if(this.figura === 'triangulo_escaleno') return 'h';
      else if(this.figura === 'triangulo_rectangulo') return 'h';
    }

    limpiarInput() {
      this.valor_1 = '';
      this.valor_2 = '';
      this.valor_3 = '';
      this.valor_4 = '';
      this.valor_5 = '';
    }

    btnGuardar() {
      this.obj = new Datos();
      this.obj.figura = this.figura;
      this.obj.operacion = this.operacion;
      this.obj.valores = [];

      if(this.operacion === 'area') {
        this.obj.formula = this.areas();

        if(this.figura === 'cuadrado') {
          this.obj.valores.push({name: 'L', valor: this.valor_1});
          this.obj.resultado = parseInt(this.valor_1) * parseInt(this.valor_1);
        } else if(this.figura === 'rectangulo') {
          this.obj.valores.push({name: 'b', valor: this.valor_1}, {name: 'a', valor: this.valor_2});
          this.obj.resultado = parseInt(this.valor_1) * parseInt(this.valor_2);
        } else if(this.figura === 'rombo') {
          this.obj.valores.push({name: 'L', valor: this.valor_1}, {name: 'D', valor: this.valor_2}, {name: 'd', valor: this.valor_3});
          this.obj.resultado = (parseInt(this.valor_2) * parseInt(this.valor_3))/2;
        } else if(this.figura === 'trapecio_isosceles') {
          this.obj.valores.push({name: 'a', valor: this.valor_1}, {name: 'b', valor: this.valor_2}, {name: 'c', valor: this.valor_3}, {name: 'h', valor: this.valor_4});
          this.obj.resultado = (parseInt(this.valor_1)+parseInt(this.valor_2))*parseInt(this.valor_4)/2;
        } else if (this.figura === 'trapecio_escaleno') {
          this.obj.valores.push({name: 'a', valor: this.valor_1}, {name: 'b', valor: this.valor_2}, {name: 'c', valor: this.valor_3}, {name: 'd', valor: this.valor_4}, {name: 'h', valor: this.valor_5});
          this.obj.resultado = (parseInt(this.valor_1)+parseInt(this.valor_2))*parseInt(this.valor_4)/2;
        } else if(this.figura === 'triangulo_equilatero') {
          this.obj.valores.push({name: 'L', valor: this.valor_1}, {name: 'h', valor: this.valor_2});
          this.obj.resultado = (parseInt(this.valor_1)*parseInt(this.valor_2))/2;
        } else if(this.figura === 'triangulo_isosceles') {
          this.obj.valores.push({name: 'L', valor: this.valor_1}, {name: 'b', valor: this.valor_2}, {name: 'h', valor: this.valor_3});
          this.obj.resultado = (parseInt(this.valor_2)*parseInt(this.valor_3))/2;
        } else if(this.figura === 'triangulo_escaleno' || this.figura === 'triangulo_rectangulo') {
          this.obj.valores.push({name: 'a', valor: this.valor_1}, {name: 'b', valor: this.valor_2}, {name: 'c', valor: this.valor_3}, {name: 'h', valor: this.valor_4});
          this.obj.resultado = (parseInt(this.valor_2)*parseInt(this.valor_4))/2;
        }
      } else {
        this.obj.formula = this.perimetros();

        if(this.figura === 'cuadrado') {
          this.obj.valores.push({name: 'L', valor: this.valor_1});
          this.obj.resultado = 4*parseInt(this.valor_1);
        } else if(this.figura === 'rectangulo') {
          this.obj.valores.push({name: 'b', valor: this.valor_1}, {name: 'a', valor: this.valor_2});
          this.obj.resultado = (2*parseInt(this.valor_1)) + (2*parseInt(this.valor_2));
        } else if(this.figura === 'rombo') {
          this.obj.valores.push({name: 'L', valor: this.valor_1}, {name: 'D', valor: this.valor_2}, {name: 'd', valor: this.valor_3});
          this.obj.resultado = 4*parseInt(this.valor_1);
        } else if(this.figura === 'trapecio_isosceles') {
          this.obj.valores.push({name: 'a', valor: this.valor_1}, {name: 'b', valor: this.valor_2}, {name: 'c', valor: this.valor_3}, {name: 'h', valor: this.valor_4});
          this.obj.resultado = parseInt(this.valor_1)+parseInt(this.valor_2)+(2*parseInt(this.valor_3))
        } else if (this.figura === 'trapecio_escaleno') {
          this.obj.valores.push({name: 'a', valor: this.valor_1}, {name: 'b', valor: this.valor_2}, {name: 'c', valor: this.valor_3}, {name: 'd', valor: this.valor_4}, {name: 'h', valor: this.valor_5});
          this.obj.resultado = parseInt(this.valor_1)+parseInt(this.valor_2)+parseInt(this.valor_3)+parseInt(this.valor_4);
        } else if(this.figura === 'triangulo_equilatero') {
          this.obj.valores.push({name: 'L', valor: this.valor_1}, {name: 'h', valor: this.valor_2});
          this.obj.resultado = 3*parseInt(this.valor_1);
        } else if(this.figura === 'triangulo_isosceles') {
          this.obj.valores.push({name: 'L', valor: this.valor_1}, {name: 'b', valor: this.valor_2}, {name: 'h', valor: this.valor_3});
          this.obj.resultado = (2*parseInt(this.valor_1))+parseInt(this.valor_2);
        } else if(this.figura === 'triangulo_escaleno' || this.figura === 'triangulo_rectangulo') {
          this.obj.valores.push({name: 'a', valor: this.valor_1}, {name: 'b', valor: this.valor_2}, {name: 'c', valor: this.valor_3}, {name: 'h', valor: this.valor_4});
          this.obj.resultado = parseInt(this.valor_1)+parseInt(this.valor_2)+parseInt(this.valor_3);
        }
      }
      this.lista_arreglo.push(this.obj);
      console.log(this.lista_arreglo);

      // Reiniciar
      this.limpiarInput();
      this.ctx.clearRect(0,0, 160, 160);
      this.flag = false;
      this.operacion = 'Elegir operación...';
      this.figura = 'Elegir figura...';
    }

    cuadricula() {
      let medida = 160;
        
      for (let x = 0; x <= medida; x = x+20){
        this.ctx.moveTo(x,0);
        this.ctx.lineTo(x,medida);
      }

      for (let y = 0; y <= medida; y = y+20){
        this.ctx.moveTo(0,y);
        this.ctx.lineTo(medida,y);
      }
        
      this.ctx.strokeStyle = "darkgrey";
      this.ctx.stroke();
    }

    // Figuras Geometricas
    tipoDeFigura() {
      if(this.figura === 'cuadrado') this.cuadrado();
      else if(this.figura === 'rectangulo') this.rectangulo();
      else if(this.figura === 'rombo') this.rombo();
      else if(this.figura === 'trapecio_escaleno') this.trapecioEscaleno();
      else if(this.figura === 'trapecio_isosceles') this.trapecioIsosceles();
      else if(this.figura === 'triangulo_equilatero') this.trianguloEquilatero();
      else if(this.figura === 'triangulo_escaleno') this.trianguloEscaleno();
      else if(this.figura === 'triangulo_isosceles') this.trianguloIsosceles();
      else if(this.figura === 'triangulo_rectangulo') this.trianguloRectangulo();
    }

    cuadrado() {
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("L", 75, 38);
      this.ctx.fillText("L", 25, 90);
      this.ctx.strokeStyle = "red";
      this.ctx.strokeRect(40,40,80,80);
    }

    rectangulo() {
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("a", 8, 85);
      this.ctx.fillText("b", 75, 138);
      this.ctx.strokeStyle = "red";
      this.ctx.strokeRect(20,40,120,80);
    }

    rombo() {
      // Rombo
      this.ctx.beginPath();
      this.ctx.moveTo(120,80);
      this.ctx.lineTo(80,40);
      this.ctx.lineTo(40,80);
      this.ctx.lineTo(80,120);
      this.ctx.strokeStyle= "red";
      this.ctx.closePath();
      this.ctx.stroke();
      // Linea horinzontal
      this.ctx.beginPath();
      this.ctx.moveTo(40, 120);
      this.ctx.lineTo(40, 130);
      this.ctx.strokeStyle= "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      this.ctx.beginPath();
      this.ctx.moveTo(40, 125);
      this.ctx.lineTo(120, 125);
      this.ctx.strokeStyle= "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      this.ctx.beginPath();
      this.ctx.moveTo(120, 120);
      this.ctx.lineTo(120, 130);
      this.ctx.strokeStyle= "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      // linea Vertical
      this.ctx.beginPath();
      this.ctx.moveTo(20, 40);
      this.ctx.lineTo(30, 40);
      this.ctx.strokeStyle= "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      this.ctx.beginPath();
      this.ctx.moveTo(25, 40);
      this.ctx.lineTo(25, 120);
      this.ctx.strokeStyle= "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      this.ctx.beginPath();
      this.ctx.moveTo(20, 120);
      this.ctx.lineTo(30, 120);
      this.ctx.strokeStyle= "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      //Texto
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("D", 8, 87);
      this.ctx.fillText("d", 70, 143);
      this.ctx.fillText("L", 105, 60);
    }

    trapecioIsosceles() {
      // Trapecio
      this.ctx.beginPath();
      this.ctx.moveTo(120,40);
      this.ctx.lineTo(40,40);
      this.ctx.lineTo(20,100);
      this.ctx.lineTo(140, 100);
      this.ctx.strokeStyle = "red";
      this.ctx.closePath();
      this.ctx.stroke();

      // Altura
      this.ctx.beginPath();
      this.ctx.moveTo(100,40);
      this.ctx.lineTo(100,100);
      this.ctx.strokeStyle = "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      //Texto
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("a", 75, 115);
      this.ctx.fillText("b", 75, 38);
      this.ctx.fillText("c", 15, 75);
      this.ctx.fillText("h", 85, 75);
    }

    trapecioEscaleno() {
      // Trapecio
      this.ctx.beginPath();
      this.ctx.moveTo(120,40);
      this.ctx.lineTo(40,40);
      this.ctx.lineTo(25,100);
      this.ctx.lineTo(150, 100);
      this.ctx.strokeStyle = "red";
      this.ctx.closePath();
      this.ctx.stroke();

      // Altura
      this.ctx.beginPath();
      this.ctx.moveTo(100,40);
      this.ctx.lineTo(100,100);
      this.ctx.strokeStyle = "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      //Texto
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("a", 75, 115);
      this.ctx.fillText("b", 75, 38);
      this.ctx.fillText("c", 15, 75);
      this.ctx.fillText("d", 140, 75);
      this.ctx.fillText("h", 85, 75);
    }

    trianguloEquilatero() {
      this.ctx.beginPath();
      this.ctx.moveTo(80,40);
      this.ctx.lineTo(40,120);
      this.ctx.lineTo(120,120);
      this.ctx.strokeStyle= "red";
      this.ctx.closePath();
      this.ctx.stroke();

      // Altura
      this.ctx.beginPath();
      this.ctx.moveTo(80,40);
      this.ctx.lineTo(80,120);
      this.ctx.strokeStyle = "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      //Texto
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("L", 105, 80);
      this.ctx.fillText("h", 67, 100);
    }

    trianguloIsosceles() {
      this.ctx.beginPath();
      this.ctx.moveTo(80,20);
      this.ctx.lineTo(40,140);
      this.ctx.lineTo(120,140);
      this.ctx.strokeStyle= "red";
      this.ctx.closePath();
      this.ctx.stroke();

      // Altura
      this.ctx.beginPath();
      this.ctx.moveTo(80,20);
      this.ctx.lineTo(80,140);
      this.ctx.strokeStyle = "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      //Texto
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("L", 105, 80);
      this.ctx.fillText("L", 45, 80);
      this.ctx.fillText("b", 77, 158);
      this.ctx.fillText("h", 67, 100);
    }

    trianguloRectangulo() {
      this.ctx.beginPath();
      this.ctx.moveTo(40,40);
      this.ctx.lineTo(40,120);
      this.ctx.lineTo(140,120);
      this.ctx.strokeStyle= "red";
      this.ctx.closePath();
      this.ctx.stroke();

      // Altura
      this.ctx.beginPath();
      this.ctx.moveTo(40,120);
      this.ctx.lineTo(85,76);
      this.ctx.strokeStyle = "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      //Texto
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("a", 25, 85);
      this.ctx.fillText("b", 85, 140);
      this.ctx.fillText("c", 100, 80);
      this.ctx.fillText("h", 74, 105);
    }

    trianguloEscaleno() {
      this.ctx.beginPath();
      this.ctx.moveTo(120,40);
      this.ctx.lineTo(20,120);
      this.ctx.lineTo(140,120);
      this.ctx.strokeStyle= "red";
      this.ctx.closePath();
      this.ctx.stroke();

      // Altura
      this.ctx.beginPath();
      this.ctx.moveTo(120,40);
      this.ctx.lineTo(120,120);
      this.ctx.strokeStyle = "blue";
      this.ctx.closePath();
      this.ctx.stroke();

      //Texto
      this.ctx.font = "bold 20px sans-serif";
      this.ctx.fillText("a", 135, 85);
      this.ctx.fillText("b", 85, 140);
      this.ctx.fillText("c", 60, 80);
      this.ctx.fillText("h", 105, 100);
    }
}
